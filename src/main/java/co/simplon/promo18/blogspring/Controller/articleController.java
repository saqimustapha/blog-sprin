package co.simplon.promo18.blogspring.Controller;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.blogspring.Entity.articleEntity;
import co.simplon.promo18.blogspring.Repository.articleRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/article")
@Validated
public class articleController {

  @Autowired
  private articleRepository repo;

  @GetMapping
  public List<articleEntity> all() {
    return repo.findAll();
  }

  @GetMapping("/{id}")
  public articleEntity one(@PathVariable @Min(1) int id) {
    articleEntity article = repo.findById(id);
    if (article == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return article;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public articleEntity add(@Valid @RequestBody articleEntity article) {
    if (article.getDate() == null) {
      article.setDate(LocalDate.now());
    }

    repo.save(article);

    return article;
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable @Min(1) int id) {
    if (!repo.delete(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/{id}")
  public articleEntity update(@Valid @RequestBody articleEntity article, @PathVariable int id) {
    if (id != article.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if (!repo.update(article)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return repo.findById(article.getId());
  }

  @PatchMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public articleEntity patch(@RequestBody articleEntity article, @PathVariable @Min(1) int id) {
    articleEntity baseAr = repo.findById(id);
    if (baseAr == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    if (article.getTitle() != null)
      baseAr.setTitle(article.getTitle());
    if (article.getContent() != null)
      baseAr.setContent(article.getTitle());
    if (article.getDate() != null)
      baseAr.setDate(article.getDate());
    if (!repo.update(baseAr))
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    return baseAr;
  }

}
