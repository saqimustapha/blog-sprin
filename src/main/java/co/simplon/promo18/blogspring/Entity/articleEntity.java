package co.simplon.promo18.blogspring.Entity;

import java.time.LocalDate;

public class articleEntity {

  private int id;
  @NotBlank
  private String title;
  @NotBlank
  private String content;
  @PastOrPresent
  private LocalDate date;
  public articleEntity() {}
  public articleEntity(String title, String content, LocalDate date) {
    this.title = title;
    this.content = content;
    this.date = date;
  }
  public articleEntity(int id, String title, String content, LocalDate date) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.date = date;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public String getContent() {
    return content;
  }
  public void setContent(String content) {
    this.content = content;
  }
  public LocalDate getDate() {
    return date;
  }
  public void setDate(LocalDate date) {
    this.date = date;
  }
}
