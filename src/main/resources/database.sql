CREATE DATABASE blog;
USE blog;
DROP TABLE IF EXISTS article; 

CREATE TABLE article(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(128),
  content TEXT,
  date DATE NOT NULL
) DEFAULT CHARSET UTF8;
 INSERT INTO article (title, content, date ) VALUES ("Titre", "Le contenu de l'article", "2022-05-04"),
 ("Titre1", "Le contenu de l'article1", "2022-04-14"),
 ("Titre2", "Le contenu de l'article2", "2022-04-28"),
 ("Titre3", "Le contenu de l'article3", "2022-05-03");
 